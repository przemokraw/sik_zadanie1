#include <unistd.h>
#include <netinet/in.h>
#include <string.h>
#include <stdio.h>

#include "message.h"


static int read_n_bytes(int sock, int n, void *buf) {
  int total = 0; // jak dużo bajtów już odczytano
  int left = n; // jak dużo bajtów trzeba jeszcze odczytać
  int readed = 0;

  while (total < n) {
    readed = read(sock, buf+total, left);
    if (readed < 0)
      return -1;
    if (readed == 0)
      return 0;
    total += n;
    left -= n;
  }
  return total;
}


static int write_n_bytes(int sock, int n, void *buf) {
  int total = 0; // jak dużo bajtów już wysłano
  int left = n; // jak dużo bajtów trzeba jeszcze wysłać
  int written = 0;

  while (total < n) {
    written = write(sock, buf+total, left);
    if (written < 0)
      return -1;
    if (written == 0)
      return 0;
    total += n;
    left -= n;
  }
  return total;
}


/* przesyła całą wiadomość. Jeśli przesłanie nie powiodło się, zwraca -1 */
int sendall(int sock, struct message *mes) {
  int rc;
  uint16_t len = htons(mes->len);
  rc = write_n_bytes(sock, (int)sizeof(mes->len), &len);
  if (rc <= 0)
    return -1;

  rc = write_n_bytes(sock, mes->len, mes->content);
  return rc;
}


/* odbiera całą wiadomość. Jeśli odbióe nie powiódł się, zwraca -1 */
int readall(int sock, struct message *mes) {
  int rc;
  uint16_t len;

  rc = read_n_bytes(sock, (int)sizeof(len), &len);
  if (rc <= 0)
    return -1;
  mes->len = ntohs(len);

  rc = read_n_bytes(sock, mes->len, mes->content);
  return rc;
}
