#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>
#include <string.h>

#include "err.h"
#include "message.h"

#define MAX_CLIENTS 20
#define DEFAULT_PORT 20160
#define TIMEOUT 3000 // czas oczekiwana dla funkcji poll

static bool end = false;

static void catch_int(int sig) {
  end = true;
}

static void init_clients(struct pollfd *client, int *active_clients) {
  int i;
  for (i = 0; i < MAX_CLIENTS+1; i++) {
    client[i].fd = -1;
    client[i].events = POLLIN;
    client[i].revents = 0;
  }
  *active_clients = 0;
}

static void clear_revents(struct pollfd *client) {
  int i;
  for (i = 0; i < MAX_CLIENTS+1; i++)
    client[i].revents = 0;
}


static void add_new_client(struct pollfd* client, int *active_clients) {
  int new_desc = accept(client[0].fd, NULL, NULL);
  int i;
  if (new_desc < 0) {
    return;
  }
 
  /* szukanie dostępnej struktury pollfd. Założenie, że z serwerem łączy się
   * jednocześnie co najwyżej 20 klientów gwarantuje, że wolna struktura
   * zawsze się znajdzie. */
  for (i = 1; i < MAX_CLIENTS+1; i++) {
    if (client[i].fd == -1) {
      client[i].fd = new_desc;
      (*active_clients) += 1;
      break;
    }
  }
}


/* przesyła wiadomośc do wszystkich klientów z wyjątkiem klienta nr exc */
static void send_to_all_except(struct pollfd *client, int exc, struct message *m) {
  int i;
  for (i = 1; i < MAX_CLIENTS+1; i++) {
    if (i == exc)
      continue;
    sendall(client[i].fd, m);
  }
}


int main(int argc, char **argv) {
  struct pollfd client[MAX_CLIENTS+1]; // +1 na serwer
  int active_clients;
  struct sockaddr_in server;
  int port;
  int rc, i;
  struct message mes[MAX_CLIENTS];
  int left_bytes[MAX_CLIENTS]; // ile bajtów zostało do odebrania całej wiadomości
                               // jeśli ustawione na -1, to będziemy wczytywać
                               // długość wiadomości
  int readed_bytes[MAX_CLIENTS]; // ile bajtów już odczytano
  bool reading_length[MAX_CLIENTS]; // czy wczytujemy długość, czy treść wiadomości?

  if (argc > 2) {
    printf("Użycie: ./server [port]\n\n"
           "Parametry:\n\n"
           "port - opcjonalny numer portu, na którym serwer ma odbierać "
           "połączenia od klientów (liczba dziesiętna); jeśli nie podano "
           "argumentu, jako numer portu zostanie przyjęta liczba 20160.\n");
    return 1;
  }

  signal(SIGINT, catch_int);

  init_clients(client, &active_clients);  
  client[0].fd = socket(PF_INET, SOCK_STREAM, 0);
  if (client[0].fd < 0)
    syserr("[socket]");

  /* ustalenie numeru portu */
  port = argc == 2? atoi(argv[1]) : DEFAULT_PORT;

  server.sin_family = AF_INET;
  server.sin_addr.s_addr = htonl(INADDR_ANY);
  server.sin_port = htons(port);

  rc = bind(client[0].fd, (struct sockaddr*)&server, (socklen_t)sizeof(server));
  if (rc < 0)
    syserr("[bind]");

  rc = listen(client[0].fd, 20);
  if (rc < 0)
    syserr("[listen]");

  /* inicjalizacja danych o wiadomościach od klientów */
  for (i = 0; i < MAX_CLIENTS; i++) {
    left_bytes[i] = (int)sizeof(uint16_t); // najpierw będziewmy wczytwać długość wiadomości
    readed_bytes[i] = 0;
    reading_length[i] = true;
  }

  do {
    clear_revents(client);

    /* otrzymano sygnał zakończenia */
    if (end && client[0].fd >= 0) {
      for (i = 0; i < MAX_CLIENTS+1; i++) {
        if (client[i].fd >= 0) 
          if (close(client[i].fd) < 0)
            syserr("[close]");
        client[i].fd = -1;
        active_clients -= 1;
      }
    }

    rc = poll(client, MAX_CLIENTS+1, TIMEOUT);
    if (rc > 0) {
      if (!end && (client[0].revents & POLLIN))
        add_new_client(client, &active_clients);

      for (i = 1; i < (MAX_CLIENTS+1); i++) {
        if (client[i].fd == -1)
          continue;

        printf("Klient %d\n", i);

        if (client[i].revents & (POLLIN | POLLERR)) {
          if (reading_length[i]) {
            printf("Wczytuję długość\n");
            rc = read(client[i].fd, &mes[i].len + readed_bytes[i], left_bytes[i]);
          }
          else {
            printf("Wczytuję wiadomość\n");
            rc = read(client[i].fd, &mes[i].content + readed_bytes[i], left_bytes[i]);
          }


          /* błędne dane lub koniec transmisji */
          if (rc <= 0) {
            if (close(client[i].fd) < 0)
              syserr("[close]");
            client[i].fd = -1;
            active_clients -= 1;
          }
          /* wczytano jakieś dane, sprawdam, czy to koniec wczytywania i można
           * zacząć wczytwyać treść wiadomości lub ją wyświetlić */
          else {
            readed_bytes[i] += rc;
            left_bytes[i] -= rc;
            printf("Wczytano %d danych. left = %d, readed = %d\n", rc, left_bytes[i], readed_bytes[i]);
            if (left_bytes[i] == 0) {
              /* koniec wczytwyania długości wiadomości */
              if (reading_length[i]) {
                printf("Przestaję wczytywać długość\n");
                reading_length[i] = false;
                mes[i].len = ntohs(mes[i].len);
                left_bytes[i] = (int)mes[i].len;
                readed_bytes[i] = 0;
              }
              /* koniec wczytiwania treści wiadomości */
              else {
                printf("Wysyłam wiadomość\n");
                reading_length[i] = true;
                left_bytes[i] = (int)sizeof(uint16_t);
                readed_bytes[i] = 0;
                send_to_all_except(client, i, &mes[i]);
                memset(&mes[i], 0, sizeof(mes[i]));
              }
            }
          }
        }
      }
        
    }
  } while(!end || active_clients > 0);

  if (client[0].fd >= 0)
    close(client[0].fd);
    
  return 0;
}
