#include <poll.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>

#include "err.h"
#include "message.h"


char * DEFAULT_PORT =  "20160";
static bool end = false;


static void catch_int(int sig) {
  end = true;
}


/* czyta jedną linię ze standardowego wejścia i przetwarza ją do postaci
 * nadającej się do wysłania */
static int prepare_message(struct message *m) {
  char *line = NULL;
  size_t n = 0;
  int len;

  len = getline(&line, &n, stdin);
  if (len < 1) {
    free (line);
    return -1;
  }

  if (line[0] == '\n') {
      free(line);
      return -1;
  }

  m->len = len > MAX_MESSAGE_LEN ? MAX_MESSAGE_LEN : len-1;
  strncpy(m->content, line, m->len);

  free(line);
  return 0;
}


int main(int argc, char **argv) {
  struct pollfd pfds[2]; // pierwszy dla stdin, drugi dla serwera
  int sock;
  struct addrinfo hints, *res;
  int rc;
  char port[6];
  struct message mes;
  int left_bytes; // ile bajtów zostało do odebranania?
  int readed_bytes; // ile bajtów już odebrano?
  bool read_length; // true, jeśli wczytujemy długość wiadomości,
                       // false, jeśli zawartość
  
  if (argc < 2 || argc > 3) {
    printf("Użycie: ./client host [port]\n\n"
           "Parametry:\n\n"
           "host - nazwa serwera, z którym należy się połączyć;\n"
           "port - opcjonalny numer portu serwera, z którym należy się "
           "połączyć (liczba dziesiętna); jeśli nie podano argumentu, "
           "jako numer portu zostanie przujęta liczba 20160\n");
    return 1;
  }

  signal(SIGINT, catch_int);

  sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (sock < 0)
      syserr("[socket]");

  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_flags = 0;
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = IPPROTO_TCP;

  /* ustalenie numeru portu */
  sprintf(port, "%s", argc == 3 ? argv[2] : DEFAULT_PORT);

  rc = getaddrinfo(argv[1], port, &hints, &res);
  if (rc != 0)
      syserr("[getaddrinfo]");
  
  if (connect(sock, res->ai_addr, res->ai_addrlen) != 0)
      syserr("[connect]");
  freeaddrinfo(res);

  /* zainicjowanie struktur pollfd */
  pfds[0].fd = 0;
  pfds[0].events = POLLIN;
  pfds[0].revents = 0;

  pfds[1].fd = sock;
  pfds[1].events = POLLIN;
  pfds[1].revents = 0;

  /* inicjalizacja dancyh o wiadomościach od serwera */
  left_bytes = (int)sizeof(uint16_t); // najpierw wczytujemy długość wiadomości
  readed_bytes = 0;
  read_length = true;

  do {
    rc = poll(pfds, 2, 5000);
    printf("rc = %d\n", rc);

    if (rc < 0) {
      if (close(pfds[1].fd) < 0) 
        syserr("[close]");
      if (close(pfds[0].fd) < 0)
        syserr("[close]");
      return 100;
    }

    if (!end && (rc > 0)) {
      if (pfds[0].revents & POLLIN) {
        if (prepare_message(&mes) == 0)
          rc = sendall(pfds[1].fd, &mes);
      }
      else if (pfds[1].revents & POLLIN) {
        if (read_length)
          rc = read(pfds[1].fd, &mes.len + readed_bytes, left_bytes);
        else
          rc = read(pfds[1].fd, &mes.content + readed_bytes, left_bytes);

        if (rc <= 0) {
          if (close(pfds[1].fd) < 0)
            syserr("[close]");
          if (close(pfds[0].fd) < 0)
            syserr("[close]");
          return 100;
        }
        else {
          readed_bytes += rc;
          left_bytes -= rc;
          if (left_bytes == 0) {
            if (read_length) {
              read_length = false;
              mes.len = ntohs(mes.len);
              mes.content[mes.len] = '\0';
              left_bytes = (int)mes.len;
              readed_bytes = 0;
            }
            else {
              read_length = true;
              left_bytes = (int)sizeof(uint16_t);
              readed_bytes = 0;
              printf("%s\n", mes.content);
              fflush(stdout);
              memset(&mes, 0, sizeof(mes));
            }
          }
        }
      }
      else {
          printf("nic\n");
      }
    }
  } while(!end);

  if (close(pfds[1].fd) < 0)
    syserr("[close]");
  if (close(pfds[0].fd) < 0)
    syserr("[close]");
  return 0;
}
