#ifndef _MESSAGE_H
#define _MESSAGE_H

#include <inttypes.h>

#define MAX_MESSAGE_LEN 1000

struct message {
  uint16_t len;
  char content[MAX_MESSAGE_LEN+1];
} __attribute__((packed));

extern int sendall(int sock, struct message *mes);

extern int readall(int sock, struct message *mes);

#endif
