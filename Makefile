CC = gcc
CFLAGS = -Wall -O2
LFLAGS = -Wall
TARGETS = client server 

all: $(TARGETS) 

client: client.o message.o err.o
	$(CC) $(LFLAGS) $^ -o $@

server: server.o message.o err.o
	$(CC) $(LFLAGS) $^ -o $@

clean:
	rm -f *.o $(TARGETS) 
